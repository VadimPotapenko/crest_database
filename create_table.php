<?php
$DB_CONF = require_once __DIR__.'/db_conf.php';
$mysqli = new mysqli($DB_CONF['host'], $DB_CONF['username'], $DB_CONF['password'], $DB_CONF['dbname']);
if ($mysqli->connect_errno) die($mysqli->connect_error);

$sql_tables = array(
	'CREATE TABLE `clients` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`access_token` varchar(255) NOT NULL,
		`expires_in` varchar(255) NOT NULL,
		`application_token` varchar(255) NOT NULL,
		`refresh_token` varchar(255) NOT NULL,
		`domain` varchar(255) NOT NULL,
		`client_endpoint` varchar(255) NOT NULL,
		`date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`)
	)',
	'CREATE TABLE `app_info` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`client_id` int(11) NOT NULL,
		`title` varchar(255) NOT NULL,
		`count_free_act` int(11) NULL,
		`trial` int(1) NULL,
		`date_last_free_act` datetime NULL,
		`active` int(1) NULL,
		`date_install` datetime NULL,
		`date_disable` datetime NULL,
		PRIMARY KEY (`id`),
		FOREIGN KEY (`client_id`) REFERENCES clients(`id`)
	)',
	'CREATE TABLE `app_log` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`client_id` int(11) NOT NULL,
		`app_id` int(11) NOT NULL,
		`message` text NULL,
		`date` datetime NULL,
		PRIMARY KEY (`id`),
		FOREIGN KEY (`client_id`) REFERENCES clients(`id`),
		FOREIGN KEY (`app_id`) REFERENCES app_info(`id`)
	)'
);

// foreach ($sql_tables as $command)
// 	if (!$result = $mysqli->query($command)) die($mysqli->error);
// $sql = 'SHOW TABLES;';

// if (!$result = $mysqli->query($sql)) die($mysqli->error);
// while( $row = mysqli_fetch_assoc($result))
//     $data[] = $row;
Debugger::debug($data);