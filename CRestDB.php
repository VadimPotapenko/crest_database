<?php
require_once __DIR__.'/idiorm.php';

/**
* @version 1.0
* Обноволенный класс CRest с возможностью работы с бд (сохранение данных ключей доступа к порталу в базе)
* 	Обладет теми же свойствами и методами что и ванильный CRest и расширение CRestPlus
* 	За исключением возможности работать с вебхуками
* 	Добавлен метод restCommand работает с портал без надобности установки приложения, но ограничен правами обращающегося пользователя
* 
* @author NiceDo 04.03.2020
*/
class CRestDB {
	const BATCH_COUNT    = 50;      // количество запросов в 1 батч-пакете
	const TYPE_TRANSPORT = 'json'; // json & xml транспрот
	const TABLES = array('clients' => 'clients', 'log' => 'app_log', 'app' => 'app_info');
	public $currentClient;       // храним текущего клиента

	/**
	* Конструктор класса, собираем данные ТЕКУЩЕГО клиента из post запроса
	*
	* @return void
	*/
	public function __construct () {
		if (isset($_REQUEST['auth']) && !empty($_REQUEST['auth'])) {
			$this->currentClient = array(
				'access_token'      => htmlspecialchars($_REQUEST['auth']['access_token']),
				'expires_in'        => htmlspecialchars($_REQUEST['auth']['expires_in']),
				'application_token' => htmlspecialchars($_REQUEST['auth']['application_token']),
				'refresh_token'     => htmlspecialchars($_REQUEST['auth']['refresh_token']),
				'domain'            => htmlspecialchars($_REQUEST['auth']['domain']),
				'client_endpoint'   => htmlspecialchars($_REQUEST['auth']['client_endpoint'])
			);
		} elseif (isset($_REQUEST['PLACEMENT']) && $_REQUEST['PLACEMENT'] == 'DEFAULT') {
			$this->currentClient = array(
				'access_token'      => htmlspecialchars($_REQUEST['AUTH_ID']),
				'expires_in'        => htmlspecialchars($_REQUEST['AUTH_EXPIRES']),
				'application_token' => htmlspecialchars($_REQUEST['APP_SID']),
				'refresh_token'     => htmlspecialchars($_REQUEST['REFRESH_ID']),
				'domain'            => htmlspecialchars($_REQUEST['DOMAIN']),
				'client_endpoint'   => 'https://'.htmlspecialchars($_REQUEST['DOMAIN']).'/rest/'
			);
		}
	}

	/**
	* Инсталяция приложения, осуществляет проверку на дубликат и сохранение данных в бд
	*
	* @return array данные клиента
	*/
	public function installApp () {
		if (isset($this->currentClient['domain']) && !empty($this->currentClient['domain'])) {
			$current = $this->getClient($this->currentClient['domain']);
			if (!isset($current[0]['id'])) {
				return $this->saveNewClient($this->currentClient); 
			}
		}
		return false;
	}

	/**
	* Подключение к бд средствами ORM
	*
	* @return boolean
	*/
	private function connectDb () {
		$file = __DIR__.'/db_conf.php';
		if (file_exists($file)) $db_conf = require $file;
		else return false;

		$test = ORM::configure(array(
			'connection_string' => 'mysql:host='.$db_conf['host'].';dbname='.$db_conf['dbname'],
			'username'          => $db_conf['username'],
			'password'          => $db_conf['password']
		));
		return true;
	}

	/**
	* Сохранение данных клиента в бд
	*
	* @var array auth - данные клиента
	* @return array - свойства объекта ORM с данными клиента
	*/
	private function saveNewClient ($auth) {
		if ($this->connectDb()) {
			$new_client = ORM::for_table('clients')->create();
			$new_client->access_token      = $auth['access_token'];
			$new_client->expires_in        = $auth['expires_in'];
			$new_client->application_token = $auth['application_token'];
			$new_client->refresh_token     = $auth['refresh_token'];
			$new_client->domain            = $auth['domain'];
			$new_client->client_endpoint   = $auth['client_endpoint'];
			$new_client->save();
			return $new_client;
		}
	}

	/**
	* Метод передачи данных Api средствами библиотеки curl
	*
	* @var array arParams - массив данных для передачи
	* @return array - массив ответа метода рест апи битрикса
	*/
	protected function callCurl ($arParams) {
		if (!function_exists('curl_init')) {
			return array(
				'error'             => 'error_php_lib_curl',
				'error_information' => 'need install curl lib'
			);
		}

		$arSettings = $this->getClient($this->currentClient['domain'])[0];
		if (isset($arSettings['id']) && !empty($arSettings['id'])) {
			if (isset($arParams['this_auth']) && $arParams[ 'this_auth' ] == 'Y') {
				$url = 'https://oauth.bitrix.info/oauth/token/';

			} else {
				$url = $arSettings["client_endpoint"].$arParams['method'].'.'.static::TYPE_TRANSPORT;
				if (empty($arSettings[ 'is_web_hook' ]) || $arSettings[ 'is_web_hook' ] != 'Y') {
					$arParams[ 'params' ][ 'auth' ] = $arSettings[ 'access_token' ];
				}
			}

			$sPostFields = http_build_query($arParams[ 'params' ]);
			try {
				$obCurl = curl_init();
				curl_setopt($obCurl, CURLOPT_URL, $url);
				curl_setopt($obCurl, CURLOPT_RETURNTRANSFER, true);

				if($sPostFields){
					curl_setopt($obCurl, CURLOPT_POST, true);
					curl_setopt($obCurl, CURLOPT_POSTFIELDS, $sPostFields);
				}

				curl_setopt(
					$obCurl, CURLOPT_FOLLOWLOCATION, (isset($arParams[ 'followlocation' ]))
					? $arParams[ 'followlocation' ] : 1
				);
				if(defined("C_REST_IGNORE_SSL") && C_REST_IGNORE_SSL === true){
					curl_setopt($obCurl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($obCurl, CURLOPT_SSL_VERIFYHOST, false);
				}
				$out = curl_exec($obCurl);
				$info = curl_getinfo($obCurl);
				if(curl_errno($obCurl))$info[ 'curl_error' ] = curl_error($obCurl);

				if(static::TYPE_TRANSPORT == 'xml' && (!isset($arParams[ 'this_auth' ]) || $arParams[ 'this_auth' ] != 'Y')) $result = $out;
				else $result = $this->expandData($out);
				curl_close($obCurl);

				if(!empty($result[ 'error' ])){
					if($result[ 'error' ] == 'expired_token' && empty($arParams[ 'this_auth' ])) {
						$result = $this->GetNewAuth($arParams);
					} else {
						$arErrorInform = array(
							'expired_token'          => 'expired token, cant get new auth? Check access oauth server.',
							'invalid_token'          => 'invalid token, need reinstall application',
							'invalid_grant'          => 'invalid grant, check out define C_REST_CLIENT_SECRET or C_REST_CLIENT_ID',
							'invalid_client'         => 'invalid client, check out define C_REST_CLIENT_SECRET or C_REST_CLIENT_ID',
							'QUERY_LIMIT_EXCEEDED'   => 'Too many requests, maximum 2 query by second',
							'ERROR_METHOD_NOT_FOUND' => 'Method not found! You can see the permissions of the application: CRest::call(\'scope\')',
							'NO_AUTH_FOUND'          => 'Some setup error b24, check in table "b_module_to_module" event "OnRestCheckAuth"',
							'INTERNAL_SERVER_ERROR'  => 'Server down, try later'
						);
						if(!empty($arErrorInform[ $result[ 'error' ] ])) $result[ 'error_information' ] = $arErrorInform[ $result[ 'error' ] ];
					}
				}
				if(!empty($info[ 'curl_error' ])){
					$result[ 'error' ] = 'curl_error';
					$result[ 'error_information' ] = $info[ 'curl_error' ];
				}
				return $result;

			} catch (Exception $e) {
				return array ('error' => 'exception', 'error_information' => $e -> getMessage(),);
			}
			return array('error' => 'no_install_app', 'error_information' => 'error install app, pls install local application ');
		}
	}

	/**
	* Метод обновления данных клиента в бд при протухании токенов
	*
	* @var array arParams - массив данных, который не удалось передать при протухшем токене
	* @return array - массив ответа метода рест апи битрикса
	*/
	public function GetNewAuth ($arParams) {
		$result = array();
		$client_secret_key = require __DIR__.'/setting.php';
		$arSettings = $this->getClient($this->currentClient['domain'])[0];
		if ($arSettings !== false) {
			$arParamsAuth = array(
				'this_auth' => 'Y',
				'params'    => array(
					'client_id'     => $client_secret_key[ 'C_REST_CLIENT_ID' ],
					'grant_type'    => 'refresh_token',
					'client_secret' => $client_secret_key[ 'C_REST_CLIENT_SECRET' ],
					'refresh_token' => $arSettings[ "refresh_token" ],
				)
			);

			$newData = $this->callCurl($arParamsAuth);
			if(isset($newData[ 'C_REST_CLIENT_ID' ])) unset($newData[ 'C_REST_CLIENT_ID' ]);
			if(isset($newData[ 'C_REST_CLIENT_SECRET' ])) unset($newData[ 'C_REST_CLIENT_SECRET' ]);
			if(isset($newData[ 'error' ])) unset($newData[ 'error' ]);
			$this->currentClient['access_token']  = $newData['access_token'];
			$this->currentClient['expires_in']    = $newData['expires_in'];
			$this->currentClient['refresh_token'] = $newData['refresh_token'];

			$updateClient = ORM::for_table('clients')->where('domain', $this->currentClient['domain'])->find_one();
			$updateClient->set(
				array(
					'access_token'      => $this->currentClient['access_token'],
					'expires_in'        => $this->currentClient['expires_in'],
					'application_token' => $this->currentClient['application_token'],
					'refresh_token'     => $this->currentClient['refresh_token'],
					'domain'            => $this->currentClient['domain'],
					'client_endpoint'   => $this->currentClient['client_endpoint']
				)
			);
			$updateClient->save();
			$arParams[ 'this_auth' ] = 'N';
			$result = $this->callCurl($arParams);
		}
		return $result;
	}

	/**
	* json ответ апи переводим в массив
	*
	* @var object data - объект который переводим в массив
	* @return array - преобразованный массив
	*/
	protected function expandData($data) {
		$return = json_decode($data, true);
		return $return;
	}

	/**
	* Получаем данные клиента из бд (дублируется с методом checkClient)
	*
	* @var string domain - домен клиента
	* @return array - данные клиента из бд в виде массива
	*/
	private function getClient ($domain) {
		if ($this->connectDb()) {
			$client = ORM::for_table('clients')->where('domain', $domain)->find_array();
			if(isset($client[0]['id']) && !empty($client[0]['id'])) return $client;
		}
		return false;
	}

	/**
	* Традиционный метод call
	*
	* @var string method - метод рест апи к которому обращаемся
	* @var array params - данные которые передаем методу рест апи
	* @return array - массив ответа метода рест апи битрикс
	*/
	public function call ($method, $params = array()) {
		$arPost = array(
			'method' => $method,
			'params' => $params
		);
		$result = static::callCurl($arPost);
		return $result;
	}

	/**
	* Традиционный метод callBatch
	*
	* @var array arData - массив с данными и методом
	* @return array - массив ответа метода батч
	*/
	public function callBatch ($arData, $halt = 0) {
		$arResult = array();
		if (is_array($arData)) {
			$arDataRest = array();
			$i = 0;
			foreach ($arData as $key => $data) {
				if (!empty($data['method'])) {
					$i++;
					if (static::BATCH_COUNT > $i) {
						$arDataRest['cmd'][$key] = $data['method'];
						if (!empty($data['params'])) {
							$arDataRest['cmd'][$key] .= '?'.http_build_query($data['params']);
						}
					}
				}
			}

			if (!empty($arDataRest)) {
				$arDataRest['halt'] = $halt;
				$arPost = array(
					'method' => 'batch',
					'params' => $arDataRest
				);
				$arResult = $this->callCurl($arPost);
			}
		}
		return $arResult;
	}

	/**
	* Функция посчитывает количество необходимых сущностей на портале и создает массив с параметрами получения
	* всего списка этих сущностей
	*
	* @var str method - метод rest
	* @var arr params - массив, параметры для списочных методов (filter, select)
	* @return arr - массив с для batch запроса, разделенный по 50 пакетов 
	*/
	protected function iteration ($method, $params) {
		$data = false;
		$tmp = $this->call($method, $params);
		$iteration = intval($tmp['total'] / 50) + 1;
		if ($tmp['total'] % 50 == 0) $iteration -= 1;
		for ($i = 0; $i < $iteration; $i++) {
			$start = $i * 50;
			$data[$i]['method'] = $method;
			$data[$i]['params'] = array('start' => $start);
			$data[$i]['params'] += $params;
		}

		if (isset($data)) {
			if (count($data) > 50) $data = array_chunk($data, 50);
			else $data = array($data);
		}
		return $data;
	}

	/**
	* Функция для списочных методов, получает весь список сущностей, соблюдая условия фильтра
	*
	* @var str method - списочный метод rest
	* @var arr params - параметры для списочных методов (filter, select)
	* @return arr - результат метода callbatch (список сущностей) или error
	*/
	public function callBatchList ($method, $params) {
		$tmp = $this->iteration($method, $params);
		if (!empty($tmp)) {
			for ($i = 0, $s = count($tmp); $i < $s; $i++) {
				$result[] = $this->callBatch($tmp[$i]);
			}
		}
		return $result ?: false;
	}

	/**
	* Функция для получения данных пользователей, принимает простой массив id ('1','2','3','n'),
	* подходит для случаев, когда нужно получить данные пользователей связанных с определенными событиями(лиды, сделки),
	* для получения пользователей (или пользователя) с определенными фильтрами или ограничениями лучше использовать callbatch
	*
	* @var arr params - массив id ('1','2','3','n')
	* @return arr - результат метода callbacth (данные пользователей)
	*/
	public function callBatchUsers ($ids) {
		foreach ($ids as $id) {
			$data[] = array('method' => 'user.get', 'params' => array('ID' => $id));
		}
		$size = count($data);
		if ($size > 50) $data = array_chunk($data, 50);
		else $data = array($data);
		for ($i = 0; $i < $size; $i++) {
			$return[] = $this->callBatch($data[$i]);
		}
		return $return ?: false;
	}

	/**
	* Метод обращается к рест апи битрикса токенами текущого пользователя
	* Отлично подходит для получения данных пользователя который обратился на портал
	*
	* @var string method - метод рест апи
	* @var array params - массив данных передаваемый методу
	* @return array - массви ответа метода рест апи битрикс
	*/
	public function restCommand ($method, $params = array()) {
		$queryUrl = $this->currentClient['client_endpoint'].$method;
		$queryData = http_build_query(array_merge($params, $this->currentClient));
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_POST           => 1,
			CURLOPT_HEADER         => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => 1,
			CURLOPT_URL            => $queryUrl,
			CURLOPT_POSTFIELDS     => $queryData
		));
		$result = curl_exec($curl);
		curl_close($curl);
		$return = json_decode($result, 1);
		return $return ?: false;
	}

	/**
	* Получаем данные пользователя обратившегося на портал (можно использовать в ограничении прав пользователей в приложении)
	*
	* @return array - данные текущего пользователя типа array('admin' => true/false, 'user' => dataCurrentUser)
	*/
	public function getCurrentUser () {
		$isAdmin = $this->restCommand('user.admin', array());
		$user    = $this->restCommand('user.current', array());
		$return = array('isadmin' => $isAdmin['result'], 'user' => $user['result']);
		return $return ?: false;
	}

	/**
	* Метод выводит системную информацию о сущности битрикса Дело
	*
	* @return array - массив с ссимной информацией сущности дело битрикса
	*/
	public function aboutActivity () {
		$data = array(
			'fields'       => array('method' => 'crm.activity.fields', 'params' => array()),
			'direction'    => array('method' => 'crm.enum.activitydirection', 'params' => array()),
			'ownertype'    => array('method' => 'crm.enum.ownertype', 'params' => array()),
			'status'       => array('method' => 'crm.enum.activitystatus', 'params' => array()),
			'activitytype' => array('method' => 'crm.enum.activitytype', 'params' => array())
		);

		$tmp = $this->callBatch($data);
		foreach ($tmp['result']['result'] as $key => $value) {
			$return[$key] = $value;
		}
		return $return ?: false;
	}
}